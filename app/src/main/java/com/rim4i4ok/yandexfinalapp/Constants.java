package com.rim4i4ok.yandexfinalapp;

import net.HostsProvider;

/**
 * Created by Rim4i4ok on 28.09.2015.
 */
public class Constants {

    static final String OAUTH_URL =  "https://m.money.yandex.ru/oauth/authorize";
    static final String TOKEN_URL = "https://m.money.yandex.ru/oauth/token";
    static final String ACCOUNT_INFO_URL = "https://money.yandex.ru/api/account-info";
    static final String OPERATION_HISTORY_URL = "https://money.yandex.ru/api/request-payment";


    static final String CLIENT_ID = "1282A59D7A2D85154FC9C010A7612654E46905E26755F5C9BC2839B7E8E890AD";
    static final String CLIENT_SECRET ="0620F288CAE722580496E06AF9ECE7ECD8A5982425A77944AEB93281EC16964E0BEA56F06AC672A1BDB6DB6DCD6F90C5B9720AA72DADEE40D5808E3E4E0886C1";

    static final String RESPONSE_TYPE = "code";
    static final String REDIRECT_URI = "http://127.0.0.1";
    static final String SCOPE = "account-info";
    static final String GRANT_TYPE = " authorization_code";

}
