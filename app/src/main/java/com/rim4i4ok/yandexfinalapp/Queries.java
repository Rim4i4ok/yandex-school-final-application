package com.rim4i4ok.yandexfinalapp;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import android.util.Log;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Rim4i4ok on 28.09.2015.
 */
public class Queries {

    public static JSONObject getToken(String token) {

        List<NameValuePair> parameters = new ArrayList<NameValuePair>();
        parameters.add(new BasicNameValuePair("code", token));
        parameters.add(new BasicNameValuePair("client_id", Constants.CLIENT_ID));
        parameters.add(new BasicNameValuePair("client_secret", Constants.CLIENT_SECRET));
        parameters.add(new BasicNameValuePair("redirect_uri", Constants.REDIRECT_URI));
        parameters.add(new BasicNameValuePair("grant_type", Constants.GRANT_TYPE));
        parameters.add(new BasicNameValuePair("Content-Type", "application/x-www-form-urlencoded"));

        return getRequest(Constants.TOKEN_URL, parameters);
    }

    public static JSONObject getAccountInfo(String token){

        List<NameValuePair> parameters = new ArrayList<NameValuePair>();

        parameters.add(new BasicNameValuePair("Host:", "money.yandex.ru"));
        parameters.add(new BasicNameValuePair("Authorization:", "Bearer " + token.toString()));
        parameters.add(new BasicNameValuePair("Content-Type:", "application/x-www-form-urlencoded"));

        parameters.add(new BasicNameValuePair("Content-Length:", "0"));

        return getRequest(Constants.ACCOUNT_INFO_URL, parameters);
    }

    public static JSONObject getRequest(String uri, List<NameValuePair> parameters){
        JSONObject result = null;
        String json = "";

        try {
            URL url = new URL(uri);
            HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

            try {
                connection.setReadTimeout(10000);
                connection.setConnectTimeout(15000);
                connection.setRequestMethod("POST");

                connection.setUseCaches(false);
                connection.setDoInput(true);
                connection.setDoOutput(true);


                OutputStream post = connection.getOutputStream();
                UrlEncodedFormEntity entity = new UrlEncodedFormEntity(parameters);
                entity.writeTo(post);
                post.flush();

                InputStream inputStream = connection.getInputStream();
                json = getString(inputStream);
            }
            catch (Exception exception){
                exception.printStackTrace();
            }
            finally {
                if(connection != null){
                    connection.disconnect();
                }
            }
        }
        catch (Exception exception){
            exception.printStackTrace();
        }

        try {
            result = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        return result;
    }

    private static String getString(InputStream inputStream) throws IOException {
        // iso-8859-1
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line + "\n");
        }
        inputStream.close();

        return stringBuilder.toString();
    }


}
