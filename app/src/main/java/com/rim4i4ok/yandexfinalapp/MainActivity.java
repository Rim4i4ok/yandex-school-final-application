package com.rim4i4ok.yandexfinalapp;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends Activity {

    Dialog auth_dialog;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPreferences = getSharedPreferences("AppPref", MODE_PRIVATE);

        Button btn_auth = (Button)findViewById(R.id.auth);
        btn_auth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                auth_dialog = new Dialog(MainActivity.this);
                auth_dialog.setContentView(R.layout.wv_auth);

                WebView wv_auth = (WebView) auth_dialog.findViewById(R.id.webv);
                wv_auth.getSettings().setJavaScriptEnabled(true);
                wv_auth.loadUrl(Constants.OAUTH_URL
                        + "?redirect_uri=" + Constants.REDIRECT_URI
                        + "&response_type=" + Constants.RESPONSE_TYPE
                        + "&client_id=" + Constants.CLIENT_ID
                        + "&scope=" + Constants.SCOPE);
                wv_auth.setWebViewClient(new WebViewClient() {

                    boolean authComplete = false;
                    Intent resultIntent = new Intent();

                    @Override
                    public void onPageStarted(WebView view, String url, Bitmap favicon) {
                        super.onPageStarted(view, url, favicon);
                    }

                    String authCode;

                    @Override
                    public void onPageFinished(WebView view, String url) {
                        super.onPageFinished(view, url);

                        if (url.contains("?code=") && authComplete != true) {
                            Uri uri = Uri.parse(url);
                            authCode = uri.getQueryParameter("code");
                            authComplete = true;
                            resultIntent.putExtra("code", authCode);
                            MainActivity.this.setResult(Activity.RESULT_OK, resultIntent);
                            setResult(Activity.RESULT_CANCELED, resultIntent);

                            SharedPreferences.Editor edit = sharedPreferences.edit();
                            edit.putString("Code", authCode);
                            edit.commit();

                            auth_dialog.dismiss();
                            new TokenGet().execute();

                        } else if (url.contains("error=access_denied")) {
                            Log.i("", "ACCESS_DENIED_HERE");
                            resultIntent.putExtra("code", authCode);
                            authComplete = true;
                            setResult(Activity.RESULT_CANCELED, resultIntent);
                            Toast.makeText(getApplicationContext(), "Error Occured", Toast.LENGTH_SHORT).show();

                            auth_dialog.dismiss();
                        }
                    }
                });

                auth_dialog.show();
                auth_dialog.setTitle("Authorize Yandex Money");
                auth_dialog.setCancelable(true);
            }
        });

        Button btn_info = (Button)findViewById(R.id.info);
        btn_info.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                new InfoGet().execute();
            }
        });
    }

    private class TokenGet extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog pDialog;
        String Code;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Contacting Yandex Money ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            Code = sharedPreferences.getString("Code", "");
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject json = Queries.getToken(Code);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();
            if (json != null){
                try {
                    String token = json.getString("access_token");
                    Log.d("Token Access", token);

                    SharedPreferences.Editor edit = sharedPreferences.edit();
                    edit.putString("token", token);
                    edit.commit();

                    Button btn_auth = (Button)findViewById(R.id.auth);
                    btn_auth.setText("Authenticated");

                    TextView Access = (TextView)findViewById(R.id.Access);
                    Access.setText("Access Token:" + token);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            else {
                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }
    }

    private class InfoGet extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog pDialog;
        String token;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Loading Info ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            token = sharedPreferences.getString("token", "");
            pDialog.show();
        }

        @Override
        protected JSONObject doInBackground(String... args) {
            JSONObject json = Queries.getAccountInfo(token);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject json) {
            pDialog.dismiss();
            if (json != null){
                Toast.makeText(getApplicationContext(), json.toString(), Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(getApplicationContext(), "Network Error", Toast.LENGTH_SHORT).show();
                pDialog.dismiss();
            }
        }
    }
}
